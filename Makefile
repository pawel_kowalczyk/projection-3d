FLAGS := -c -g -Wall -pedantic
o_FILES := main.o simulation.o engine3D.o object3D.o camera.o plane3D.o coordDirection.o coordSystem.o wheel.o cuboid.o cube.o line3D.o rectangle.o rectangularTriangle.o triangle.o line.o point.o vector2D.o vector3D.o matrixRot.o

Engine.out: $(o_FILES) 
	g++ $(o_FILES) -lsfml-graphics -lsfml-window -lsfml-system -o Engine.out 
	rm *.o
	./Engine.out

main.o:
	g++ $(FLAGS) src/main.cpp 

simulation.o: inc/engine3D.hh
	g++ $(FLAGS) src/simulation.cpp 

engine3D.o:
	g++ $(FLAGS) src/engine3D.cpp 

object3D.o:
	g++ $(FLAGS) src/object3D.cpp 

camera.o:
	g++ $(FLAGS) src/camera.cpp 

plane3D.o:
	g++ $(FLAGS) src/plane3D.cpp 

coordDirection.o:
	g++ $(FLAGS) src/coordDirection.cpp 

coordSystem.o:
	g++ $(FLAGS) src/coordSystem.cpp 

wheel.o:
	g++ $(FLAGS) src/wheel.cpp 

cuboid.o:
	g++ $(FLAGS) src/cuboid.cpp 

cube.o:
	g++ $(FLAGS) src/cube.cpp 

line3D.o:
	g++ $(FLAGS) src/line3D.cpp 

rectangle.o:
	g++ $(FLAGS) src/rectangle.cpp 

rectangularTriangle.o:
	g++ $(FLAGS) src/rectangularTriangle.cpp 

triangle.o:
	g++ $(FLAGS) src/triangle.cpp 

line.o:
	g++ $(FLAGS) src/line.cpp 

point.o:
	g++ $(FLAGS) src/point.cpp 

vector2D.o:
	g++ $(FLAGS) src/vector2D.cpp 

vector3D.o:
	g++ $(FLAGS) src/vector3D.cpp 

matrixRot.o:
	g++ $(FLAGS) src/matrixRot.cpp 