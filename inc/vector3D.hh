#pragma once

#include "vector.hh"

class Vector3D : public Vector<double,3>{
private:

public:
    Vector3D();
    Vector3D(const Vector3D& vector) = default;
    Vector3D(const Vector<double,3>& vector);
    Vector3D(std::initializer_list<double> in);
    
};