#pragma once

#include "object3D.hh"

class Cuboid : public Object3D {

private:
    
public:

    Cuboid();
    Cuboid(CoordSystem* parent, const Vector3D& translation, const MatrixRot& rotation, const Vector3D& dimensions);
    Cuboid(CoordSystem* parent, const Vector3D& translation, const MatrixRot& rotation, const double size);

};
