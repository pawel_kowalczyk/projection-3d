#pragma once

#include <SFML/Graphics.hpp>
#include "vector2D.hh"
#include "rectangularTriangle.hh"

class Rectangle : public sf::Drawable{

private:
    RectangularTriangle _triangles[2];
    void draw(sf::RenderTarget& target, sf::RenderStates states) const;
    
public:
    Rectangle();
    Rectangle(const Point& p1, const double w, const double h);
};