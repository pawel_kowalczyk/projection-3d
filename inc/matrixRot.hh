#pragma once

#include "matrix.hh"
#include <math.h>

class MatrixRot : public Matrix<double,3>{
private:

public:
    MatrixRot(); 
    MatrixRot(const MatrixRot& mat) = default;
    MatrixRot(const Matrix<double,3>& mat);
    MatrixRot(std::initializer_list<double> in);
    MatrixRot(char axle, double angle);
};