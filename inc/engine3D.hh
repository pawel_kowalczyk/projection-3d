#pragma once

#include <iostream>
#include <cmath>
#include <SFML/Graphics.hpp>
#include "point.hh"
#include "line.hh"
#include "cuboid.hh"
#include "wheel.hh"
#include "vector3D.hh"
#include "line3D.hh"
#include "coordDirection.hh"
#include "plane3D.hh"

#define WIDTH  900
#define HEIGHT 900
#define FPS    10

struct Cuboid2D{
    std::vector<Line>  lines2D;
};

struct Wheel2D{
    std::vector<Line>  lines2D;
};

struct EventPack{
    bool isKeyAPressed = false;
    bool isKeyDPressed = false;
    bool isKeyWPressed = false;
    bool isKeySPressed = false;

    bool isKeyQPressed = false;
    bool isKeyEPressed = false;
    bool isKeyRPressed = false;
    bool isKeyFPressed = false;
    bool isKeyCTRLPressed = false;
    bool isKeySPACEPressed = false;
};

class Engine3D{

private:
    sf::ContextSettings settings;   
    sf::RenderWindow* _window;
    sf::Event _event;

    std::vector<Cuboid2D> cuboids2D;
    std::vector<Wheel2D> wheels2D;
    std::vector<Line> lines2D;
    sf::ConvexShape _plane;

    EventPack eventPack;

    double _cameraFOV;

    double Transform3DTo2D(const double xy, const double z) const;
    Vector2D Projection(Vector3D point);

public:
    Engine3D();
    Engine3D(const double cameraFOV);

    void AddToDrawQueue(const Cuboid& Cuboid3D, const sf::Color color = sf::Color::White);
    void AddToDrawQueue(const Line3D& line3D, const sf::Color color = sf::Color::White);
    void AddToDrawQueue(const Wheel& wheel3D, const sf::Color color = sf::Color::White);
    void AddToDrawQueue(const CoordDirection& coordDirection);
    void AddPlane(const Plane3D& plane3D);

    void Draw();
    void Events(); 
    void ClearRenderObjects();

    EventPack GetKeyEvents() const;
    sf::RenderWindow* GetWindow() const;
};
