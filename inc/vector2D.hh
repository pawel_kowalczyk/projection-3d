#pragma once

#include "vector.hh"
#include <SFML/Graphics.hpp>

class Vector2D : public Vector<double,2>{
private:

public:
    Vector2D();
    Vector2D(const double x, const double y);
    Vector2D(const Vector2D& vector) = default;
    Vector2D(const Vector<double,2>& vector);
    Vector2D(std::initializer_list<double> in);
    
    operator sf::Vector2f() const { return sf::Vector2f( (*this)[0], (*this)[1] ); }
    operator sf::Vertex() const { return sf::Vertex( sf::Vector2f( (*this)[0], (*this)[1] )); }
};