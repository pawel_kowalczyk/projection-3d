#pragma once

#include "matrixRot.hh"
#include "vector3D.hh"

class CoordSystem{
private:
    CoordSystem* _parent;
    Vector3D _translation;
    MatrixRot _rotation;

public:

    CoordSystem();
    CoordSystem(CoordSystem* parent, const Vector3D& translation, const MatrixRot& rotation);

    void Translate(const Vector3D& vec);

    void Rotate(const MatrixRot& matrix);

    void RotateX(const double angle);
    void RotateY(const double angle);
    void RotateZ(const double angle);

    Vector3D CalculateToParent(const Vector3D& point) const;    

    CoordSystem* GetParent() const;
    Vector3D GetTranslation() const;
    MatrixRot GetRotation() const;
};