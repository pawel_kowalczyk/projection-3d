#pragma once

#include "engine3D.hh"
#include "coordSystem.hh"
#include "cube.hh"
#include "cuboid.hh"
#include "wheel.hh"
#include "camera.hh"
#include "line3D.hh"
#include "coordDirection.hh"
#include "plane3D.hh"

class Simulation{

private:
    Engine3D _engine;
    Camera _camera;
    CoordSystem _mainCoordSystem;

    EventPack eventPack;

    // std::vector<Object3D> objects3D;

    Cuboid cube1;
    // Cuboid cube2;
    // Wheel wheel;

    // Plane3D plane3D;

    CoordDirection coordDirection;

public:
    Simulation();

    void Loop();
};