#pragma once

#include <SFML/Graphics.hpp>
#include "vector2D.hh"
#include "point.hh"
#include <array>

class Line : public sf::Drawable{

private:
    sf::Vertex _points[2]; 

    void draw(sf::RenderTarget& target, sf::RenderStates states) const;
public:
    Line();
    Line(const Point& p1, const Point& p2, const sf::Color color = sf::Color::White);
};