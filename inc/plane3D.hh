#pragma once

#include "object3D.hh"

class Plane3D : public Object3D{
private:
    
public:
    Plane3D();
    Plane3D(CoordSystem* parent, const Vector3D& translation, const MatrixRot& rotation);
    
};

