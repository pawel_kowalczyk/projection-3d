#pragma once

#include <SFML/Graphics.hpp>
#include "vector2D.hh"
#include <array>

class Point : public sf::Drawable, public Vector2D{

private:
    sf::Vertex _point;
    void draw(sf::RenderTarget& target, sf::RenderStates states) const;
public:
    Point() : Vector2D() { _point = Vector2D(); }
    Point(const Vector2D& p);
    Point(std::initializer_list<double> in);
};