#pragma once

#include "triangle.hh"

class RectangularTriangle : public Triangle{

private:

public:
    RectangularTriangle() : Triangle(){};
    RectangularTriangle(const Point& p, const double w, const double h);

};