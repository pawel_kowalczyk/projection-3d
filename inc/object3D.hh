#pragma once

#include <vector>
#include "coordSystem.hh"

class Object3D : public CoordSystem{
private:
    std::vector<Vector3D> _vertices;

protected:

    void SetVertices(std::vector<Vector3D> vertices);

public:

    Object3D() : CoordSystem() {}
    Object3D(CoordSystem* parent, const Vector3D& translation, const MatrixRot& rotation);
    Object3D(CoordSystem* parent, const Vector3D& translation, const MatrixRot& rotation, std::vector<Vector3D> vertices);
    
    std::vector<Vector3D> GetLocalVertices() const;
    std::vector<Vector3D> GetGlobalVertices() const;

};