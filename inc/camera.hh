#pragma once

#include "coordSystem.hh"

class Camera : public CoordSystem{

private:
    

public:

    Camera();
    Camera(CoordSystem* parent, const Vector3D& translation, const MatrixRot& rotation);

    double FOV_ANGLE = 45.0;
};