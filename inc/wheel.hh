#pragma once

#include <vector>
#include <cmath>
#include "object3D.hh"

class Wheel : public Object3D{
private:
    

public:
    Wheel();
    Wheel(CoordSystem* parent, const Vector3D& translation, const MatrixRot& rotation, const double size);

};

