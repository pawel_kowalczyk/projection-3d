#pragma once

#include "object3D.hh"

class Line3D : public Object3D{

private:
    
public:
    Line3D();
    Line3D(CoordSystem* parent, const Vector3D& translation, const MatrixRot& rotation, const Vector3D& p1, const Vector3D& p2);

};