#pragma once

#include "coordSystem.hh"
#include "line3D.hh"

class CoordDirection : public CoordSystem{

private:
    std::array<Line3D, 3> lines3D;

public:
    CoordDirection();
    CoordDirection(CoordSystem* parent);

    std::array<Line3D, 3> GetLines3D() const;
};
