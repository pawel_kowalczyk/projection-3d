#pragma once

#include <SFML/Graphics.hpp>
#include <array>
#include "point.hh"
#include "line.hh"
#include "../inc/vector2D.hh"

class Triangle : public sf::Drawable{

private:
    sf::Vertex _vertices[3];
    Line _lines[3];

    void draw(sf::RenderTarget& target, sf::RenderStates states) const;
public:

    Triangle();
    Triangle(const Point& p1, const Point& p2, const Point& p3);
};