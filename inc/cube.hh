#pragma once

#include "cuboid.hh"
#include "array"

class Cube : public Cuboid{

private:

public:
    Cube() : Cuboid() {}
    Cube(CoordSystem* parent, const Vector3D& translation, const MatrixRot& rotation, const double size);

};