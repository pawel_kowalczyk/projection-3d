#include "../inc/line3D.hh"

Line3D::Line3D() : Object3D() {}

Line3D::Line3D(CoordSystem* parent, const Vector3D& translation, const MatrixRot& rotation, const Vector3D& p1, const Vector3D& p2)
        : Object3D(parent, translation, rotation){

        SetVertices({p1, p2});

}