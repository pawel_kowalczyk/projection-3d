#include "../inc/coordDirection.hh"

CoordDirection::CoordDirection() : CoordSystem() {}

CoordDirection::CoordDirection(CoordSystem* parent) : CoordSystem(parent, {0,0,0}, MatrixRot('X', 0)){
    lines3D = {
        Line3D(parent, {0,0,0}, MatrixRot('X', 0), {0,0,0}, {1,0,0}),
        Line3D(parent, {0,0,0}, MatrixRot('X', 0), {0,0,0}, {0,1,0}),
        Line3D(parent, {0,0,0}, MatrixRot('X', 0), {0,0,0}, {0,0,1})
    }; 

}

std::array<Line3D, 3> CoordDirection::GetLines3D() const{
    return lines3D;
}
