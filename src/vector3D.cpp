#include "../inc/vector3D.hh"

Vector3D::Vector3D() : Vector({0,0,0}) {}

Vector3D::Vector3D(const Vector<double,3>& vector) {
    for(int i=0; i<3; i++) 
        (*this)[i] = vector[i];
}

Vector3D::Vector3D(std::initializer_list<double> in){
    std::copy(in.begin(), in.end(), getVector().begin());
} 

