#include "../inc/engine3D.hh"

Engine3D::Engine3D(){

    settings.antialiasingLevel = 8;
    _window = new sf::RenderWindow(sf::VideoMode(WIDTH, HEIGHT), "3D Engine", sf::Style::Default, settings);
    _window->setFramerateLimit(FPS);

    _cameraFOV = 45.0;
    _plane.setPointCount(4);
    _plane.setFillColor(sf::Color(128,128,128));

}

Engine3D::Engine3D(const double cameraFOV){

    settings.antialiasingLevel = 8;
    _window = new sf::RenderWindow(sf::VideoMode(WIDTH, HEIGHT), "3D Engine", sf::Style::Default, settings);
    _window->setFramerateLimit(FPS);

    _cameraFOV = cameraFOV;
    _plane.setPointCount(4);
    _plane.setFillColor(sf::Color(128,128,128));

}

double Engine3D::Transform3DTo2D(const double xy, const double z) const{

    double angleRadius = (_cameraFOV/180)*M_PI;
    return xy / ( z * tan(angleRadius/2) );

}

// point3D has to be between {-1,-1,-z} and {1,1,z}
Vector2D Engine3D::Projection(Vector3D point){


    point[1] *= -1; // Reverse Y for correct display

    point = point + Vector3D{0, 0, 10};
    point = { Transform3DTo2D( point[0], point[2]), Transform3DTo2D( point[1], point[2]), 0 };

    point = { WIDTH*(point[0] + 0.5), HEIGHT*(point[1]+0.5), 0 };

    return Vector2D{ point[0], point[1] };

}

void Engine3D::AddToDrawQueue(const Cuboid& Cuboid3D, const sf::Color color){
    
    Cuboid2D newCuboid2D;

    std::vector<Vector3D> vertices3D = Cuboid3D.GetGlobalVertices();
    std::vector<Point> vertices2D;

    for(Vector3D point3D : vertices3D){
        vertices2D.push_back(Point(Projection(point3D)));        
    }
    
    for(int i=0; i<4; ++i){
        newCuboid2D.lines2D.push_back(Line(vertices2D[i], vertices2D[(i+1)%4], color));
        newCuboid2D.lines2D.push_back(Line(vertices2D[i+4], vertices2D[(i+1)%4 + 4], color));
        newCuboid2D.lines2D.push_back(Line(vertices2D[i], vertices2D[i+4], color));
    }
    
    cuboids2D.push_back( newCuboid2D );

}

void Engine3D::AddToDrawQueue(const Wheel& wheel3D, const sf::Color color){

    Wheel2D newWheel2D;

    std::vector<Vector3D> vertices3D = wheel3D.GetGlobalVertices();
    std::vector<Point> vertices2D;

    for(Vector3D point3D : vertices3D){
        vertices2D.push_back(Point(Projection(point3D)));        
    }

    for(int i=0; i<20; ++i){
        newWheel2D.lines2D.push_back(Line(vertices2D[i], vertices2D[(i+1)%20], color));
        newWheel2D.lines2D.push_back(Line(vertices2D[i+20], vertices2D[(i+1)%20 + 20], color));
        newWheel2D.lines2D.push_back(Line(vertices2D[i], vertices2D[i+20], color));
    }

    wheels2D.push_back( newWheel2D );

}


void Engine3D::AddToDrawQueue(const Line3D& line3D, const sf::Color color){
    std::vector<Vector3D> line3DVertices = line3D.GetGlobalVertices();

    lines2D.push_back(Line(Projection(line3DVertices[0]), Projection(line3DVertices[1]), color));

}

void Engine3D::AddToDrawQueue(const CoordDirection& coordDirection){
    std::array<Line3D, 3> lines3D = coordDirection.GetLines3D();
    AddToDrawQueue(lines3D[0], sf::Color::Red);
    AddToDrawQueue(lines3D[1], sf::Color::Blue);
    AddToDrawQueue(lines3D[2], sf::Color::Yellow);
}

void Engine3D::AddPlane(const Plane3D& plane3D){
    std::vector<Vector3D> vertices = plane3D.GetGlobalVertices();

    for (size_t i = 0; i < 4; i++){
        _plane.setPoint(i, Projection(vertices[i]));
    }

}

void Engine3D::ClearRenderObjects(){
    cuboids2D.clear();
    lines2D.clear();
    wheels2D.clear();
}

void Engine3D::Events(){
    eventPack = EventPack();

    while (_window->pollEvent(_event))
    {
        if (_event.type == sf::Event::Closed)
            _window->close(); 
        if (_event.type == sf::Event::KeyPressed){
            if(_event.key.code == sf::Keyboard::A){
                eventPack.isKeyAPressed = true;
            }
            if(_event.key.code == sf::Keyboard::D){
                eventPack.isKeyDPressed = true;
            }
            if(_event.key.code == sf::Keyboard::W){
                eventPack.isKeyWPressed = true;
            }
            if(_event.key.code == sf::Keyboard::S){
                eventPack.isKeySPressed = true;
            }
            if(_event.key.code == sf::Keyboard::Q){
                eventPack.isKeyQPressed = true;
            }
            if(_event.key.code == sf::Keyboard::E){
                eventPack.isKeyEPressed = true;
            }
            if(_event.key.code == sf::Keyboard::R){
                eventPack.isKeyRPressed = true;
            }
            if(_event.key.code == sf::Keyboard::F){
                eventPack.isKeyFPressed = true;
            }
            if(_event.key.code == sf::Keyboard::LControl){
                eventPack.isKeyCTRLPressed = true;
            }
            if(_event.key.code == sf::Keyboard::Space){
                eventPack.isKeySPACEPressed = true;
            }
        }
        /// more events ///
    }

}

void Engine3D::Draw(){

    _window->clear();
    
    // _window->draw( Line({WIDTH/2, 0},  {WIDTH/2, HEIGHT}) );
    // _window->draw( Line({0, HEIGHT/2}, {WIDTH, HEIGHT/2}) );
    

    _window->draw(_plane);

    for(Cuboid2D& cube : cuboids2D){

        for(Line& line : cube.lines2D){
            _window->draw(line);
        }

    }

    for(Wheel2D& wheel : wheels2D){

        for(Line& line2D : wheel.lines2D){
            _window->draw(line2D);
        }
        
    }
    
    for(Line& line : lines2D){
        _window->draw(line);
    }

    _window->display();

}

EventPack Engine3D::GetKeyEvents() const{
    return eventPack;
}

sf::RenderWindow* Engine3D::GetWindow() const{
    return _window;
} 
