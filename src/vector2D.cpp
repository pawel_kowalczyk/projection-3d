#include "../inc/vector2D.hh"

Vector2D::Vector2D() : Vector({0,0}) {}

Vector2D::Vector2D(const double x, const double y) : Vector2D( {x,y} ){}

Vector2D::Vector2D(const Vector<double,2>& vector) {
    for(int i=0; i<2; i++) 
        (*this)[i] = vector[i];
}

Vector2D::Vector2D(std::initializer_list<double> in){
    std::copy(in.begin(), in.end(), getVector().begin());
} 


