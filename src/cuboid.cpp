#include "../inc/cuboid.hh"

Cuboid::Cuboid() : Object3D() {}

Cuboid::Cuboid(CoordSystem* parent, const Vector3D& translation, const MatrixRot& rotation, const Vector3D& dimensions)
        : Object3D(parent, translation, rotation){

    const double hw = dimensions[0]/2;
    const double hh = dimensions[1]/2;
    const double hd = dimensions[2]/2;

    std::vector<Vector3D> vertices = {
        Vector3D{-hw, hh, -hd},
        Vector3D{-hw, hh,  hd},
        Vector3D{ hw, hh,  hd},
        Vector3D{ hw, hh, -hd},

        Vector3D{-hw, -hh, -hd},
        Vector3D{-hw, -hh,  hd},
        Vector3D{ hw, -hh,  hd},
        Vector3D{ hw, -hh, -hd}
    };

    SetVertices(vertices);

}

Cuboid::Cuboid(CoordSystem* parent, const Vector3D& translation, const MatrixRot& rotation, const double size)
        : Cuboid(parent, translation, rotation, {size, size, size}){}
