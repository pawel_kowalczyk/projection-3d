#include "../inc/object3D.hh"


Object3D::Object3D(CoordSystem* parent, const Vector3D& translation, const MatrixRot& rotation) 
        : CoordSystem(parent, translation, rotation) {}


Object3D::Object3D(CoordSystem* parent, const Vector3D& translation, const MatrixRot& rotation, std::vector<Vector3D> vertices) 
        : CoordSystem(parent, translation, rotation){

    _vertices = vertices;

}

void Object3D::SetVertices(std::vector<Vector3D> vertices){
    _vertices = vertices;
}


std::vector<Vector3D> Object3D::GetLocalVertices() const{
    return _vertices;
}

std::vector<Vector3D> Object3D::GetGlobalVertices() const{
    
    std::vector<Vector3D> globalPoints;

    for(Vector3D vertex : _vertices){
        globalPoints.push_back( CalculateToParent( vertex ) );
    }

    return globalPoints;

}