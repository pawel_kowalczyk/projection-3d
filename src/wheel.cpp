#include "../inc/wheel.hh"

Wheel::Wheel() : Object3D() {}

Wheel::Wheel(CoordSystem* parent, const Vector3D& translation, const MatrixRot& rotation, const double size) 
        : Object3D(parent, translation, rotation){

    std::vector<Vector3D> vertices;

    for (size_t i = 0; i < 20; i++){
        vertices.push_back(  { size*cos((18.0*i/180)*M_PI), size*sin((18.0*i/180)*M_PI),  0.1} );
    }
    for (size_t i = 0; i < 20; i++){
        vertices.push_back( { size*cos((18.0*i/180)*M_PI), size*sin((18.0*i/180)*M_PI), -0.1} );
    }

    SetVertices(vertices);

}