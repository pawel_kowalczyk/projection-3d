#include "../inc/point.hh"

Point::Point(const Vector2D& p) : Vector2D(p) {_point = p;}

void Point::draw(sf::RenderTarget& target, sf::RenderStates states) const{
    target.draw(&_point, 1, sf::Points, states);
}

Point::Point(std::initializer_list<double> in) : Vector2D(in) {
    _point = Vector2D( (*this)[0], (*this)[1] );
} 