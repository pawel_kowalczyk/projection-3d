#include "../inc/triangle.hh"

Triangle::Triangle(){}

Triangle::Triangle(const Point& p1, const Point& p2, const Point& p3){
    _vertices[0] = p1;
    _vertices[1] = p2;
    _vertices[2] = p3;

    _lines[0] = Line(p1, p2);
    _lines[1] = Line(p2, p3);
    _lines[2] = Line(p3, p1);
}


void Triangle::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    for(int i=0; i<3; ++i)
        target.draw(_lines[i], states);

 
}