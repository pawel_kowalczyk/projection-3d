#include "../inc/simulation.hh"

Simulation::Simulation(){

    _camera = Camera( nullptr, {0,0,0}, MatrixRot('X' ,0) );
    _mainCoordSystem = CoordSystem( &_camera, {0,0,10}, MatrixRot('x', 0) );

    // _engine = Engine3D();
    // _camera.Translate({-0.5,0,0});
    cube1 = Cuboid( &_mainCoordSystem, {2,0,0}, MatrixRot('X',0), {0.4, 0.4, 1} );
    // cube2 = Cuboid( &_camera, {-0.3,-0.8,0}, MatrixRot('X',0), 0.3 );

    // wheel = Wheel( &_camera, {0,0,0}, MatrixRot('X',0), 0.5 );


    // plane3D = Plane3D( &_camera, {0,0,0}, MatrixRot('X', 0) );

    coordDirection = CoordDirection(&_mainCoordSystem);

    // cube.RotateX(15);
}

void Simulation::Loop(){
    while( _engine.GetWindow()->isOpen() ){

        _engine.Events();
        eventPack = _engine.GetKeyEvents();

        if( eventPack.isKeyAPressed )
            _mainCoordSystem.Translate({0.1, 0, 0});
        if( eventPack.isKeyDPressed )
            _mainCoordSystem.Translate({-0.1, 0, 0});
        if( eventPack.isKeyWPressed )
            _mainCoordSystem.Translate({0, -0.1, 0});
        if( eventPack.isKeySPressed )
            _mainCoordSystem.Translate({0, 0.1, 0});
        if( eventPack.isKeyCTRLPressed )
            _camera.Translate({0,0,1});
        if( eventPack.isKeySPACEPressed )
            _camera.Translate({0,0,-1});
        if( eventPack.isKeyQPressed )
            _camera.RotateY(2);
        if( eventPack.isKeyEPressed )
            _camera.RotateY(-2);
        if( eventPack.isKeyRPressed )
            _camera.RotateX(2);
        if( eventPack.isKeyFPressed )
            _camera.RotateX(-2);

        // cube1.RotateZ(1);
        cube1.RotateZ(3);
        // cube2.RotateY(1);
        // wheel.RotateZ(2);

        _engine.AddToDrawQueue(cube1);
        // _engine.AddToDrawQueue(cube2);

        // _engine.AddPlane(plane3D);
        // _engine.AddToDrawQueue(wheel);
        _engine.AddToDrawQueue(coordDirection);
        
        _engine.Draw();

        _engine.ClearRenderObjects();

    }
}