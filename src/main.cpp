#include <iostream>
#include <cmath>
#include <SFML/Graphics.hpp>
// #include "../inc/vector2D.hh"
// #include "../inc/triangle.hh"
// #include "../inc/rectangularTriangle.hh"
// #include "../inc/rectangle.hh"
// #include "../inc/vector3D.hh"
// #include "../inc/matrixRot.hh"
#include "../inc/simulation.hh"

// Vector2D Projection(const Vector3D& point){
   
//     Vector3D cameraAngles{0,M_PI/6,0};
//     Vector3D cameraPos{-10,0,10};

//     MatrixRot cameraMatrixX('X', cameraAngles[0]);
//     MatrixRot cameraMatrixY('Y', cameraAngles[1]);
//     MatrixRot cameraMatrixZ('Z', cameraAngles[2]);
//     Vector3D surfaceDistanceToCamera{0,50,100};
    
//     Vector3D d;

//     if ( cameraAngles == Vector3D{0,0,0} ){
//         d = (point-cameraPos);
//     }
//     else
//         d = cameraMatrixX*cameraMatrixY*cameraMatrixZ*(point-cameraPos);

//     Vector2D projectedPoint{
//         (surfaceDistanceToCamera[2]/d[2]) * d[0] + surfaceDistanceToCamera[0],
//         (surfaceDistanceToCamera[2]/d[2]) * d[1] + surfaceDistanceToCamera[1]
//     };
    
//     return projectedPoint;

// }

// #define WIDTH 600
// #define HEIGHT 600
// #define CUDE_DISTANCE 10
// #define FOV_ANGLE 45.0    
// Vector3D angles{15,0,0};

// double Transform3DTo2D(double xy, double z){
//     double angleRadius = (FOV_ANGLE/180)*M_PI;
//     // std::cout << angleRadius << std::endl;
//     return xy / ( z * tan(angleRadius/2) );
// }

// template <int SIZE>
// std::array<Vector2D, SIZE> Project(std::array<Vector3D,SIZE> faces3D){
    
//     std::array<Vector2D, SIZE> faces2D;

//     for( int i=0; i<SIZE; ++i ){
//         Vector3D p = faces3D[i];

//         p = MatrixRot('X', angles[0])*p;
//         p = MatrixRot('Y', angles[1])*p;
//         p = MatrixRot('Z', angles[2])*p;

//         p = p + Vector3D{0, 0, CUDE_DISTANCE};
//         p = { Transform3DTo2D( p[0], p[2] ), Transform3DTo2D( p[1], p[2] ), 0 };
//         // std::cout << p << std::endl;
//         p = { WIDTH*(p[0] + 0.5), HEIGHT*(p[1]+0.5), 0 };

//         faces2D[i] = { p[0], p[1] };
//     }

//     return faces2D;

// }

int main(){
    Simulation simulation;
    simulation.Loop();
    // // GRID

    // std::array<Vector3D, 4> grid3D = {
    //     Vector3D{-1, 0, -1},
    //     Vector3D{-1, 0, 1},
    //     Vector3D{1, 0, 1},
    //     Vector3D{1, 0, -1}
    // };

    // std::array<Line, 20> gridLines;
    // for(int i=0; i<10; ++i){
    //     gridLines[i] = Line( Point({}), Point() );
    // }
    // double gapX = fabs((grid3D[0] - grid3D[2])[0])/10;
    // double gapY = fabs((grid3D[0] - grid3D[2])[1])/10;
    // double gapZ = fabs((grid3D[0] - grid3D[2])[2])/10;
    
    // // for(int i=0; i<10; ++i){
    // //     gridPoints[i] = { Vector3D[0][0]/10, Vector3D[i]
    // // }

    // // VERTICAL PLANE
    // std::array<Vector3D, 4> plane3D = {
    //     Vector3D{-0.6, -0.3, 0},
    //     Vector3D{0.6, -0.3, 0},
    //     Vector3D{0.6, 0.3, 0},
    //     Vector3D{-0.6, 0.3, 0},
    // };
    // std::array<Vector2D, 4> plane2D;
    // std::array<Line, 4> planeLines;

    // // CUBE
    // std::array<Vector3D, 8> faces3D = {
    //     // TOP
    //     Vector3D{-0.5, -0.5, -0.5},
    //     Vector3D{-0.5, -0.5,  0.5},
    //     Vector3D{ 0.5, -0.5,  0.5},
    //     Vector3D{ 0.5, -0.5, -0.5},

    //     // BOTTOM
    //     Vector3D{-0.5, 0.5, -0.5},
    //     Vector3D{-0.5, 0.5,  0.5},
    //     Vector3D{ 0.5, 0.5,  0.5},
    //     Vector3D{ 0.5, 0.5, -0.5}
        
    // };

    // std::array<Vector2D,8> faces2D;
    // std::array<Line, 12> cubeLines;

    // // REST
    // sf::RenderWindow* window;
    // sf::Event event;

    // window = new sf::RenderWindow(sf::VideoMode(600, 600), "3D Engine");
    // window->setFramerateLimit(10);

    // // sf::Vertex point(Vector2D({150, 150}), sf::Color::Blue);
    // // sf::Vertex line[2] =
    // // {
    // //     sf::Vertex(Vector2D(10, 10)),
    // //     sf::Vertex(Vector2D(150, 150))
    // // };


    // std::vector<Triangle> _triangles;
    // std::vector<Rectangle> _rectangles;

    // _triangles.push_back( Triangle({20, 30}, {100, 60}, {120, 150}) );
    // _triangles.push_back( RectangularTriangle({300,300}, 200, 100) );
    // // _drawable.push_back( Line({400,200}, {400,400}) );

    // _rectangles.push_back( Rectangle({200,450}, 150, 50) );

    // Vector3D points3D[2] = { {0,0,0}, {250,110,200} };    
    
    // Point points2D[2];
    // Line line(points2D[0], points2D[1]);
    
    // // std::cout << points2D[0] << "," << points2D[1] << std::endl;
    

    // while (window->isOpen()){

    //     while (window->pollEvent(event))
    //     {
    //         if (event.type == sf::Event::Closed)
    //             window->close(); 

    //         /// more events ///
    //     }

       
    //     points2D[0] = Projection( points3D[0] );
    //     points2D[1] = Projection( points3D[1] );

    //     points3D[0] += {1,0,0};
    //     points3D[1] += {1,0,0};

    //     line = Line(points2D[0], points2D[1]);

    //     // std::cout << points2D[0] << ", " << points2D[1] << std::endl; 
 

    //     // CUBE
    //     angles += {0.1, 0, 0};
    //     faces2D = Project<8>(faces3D);
    //     for(int i=0; i<4; ++i){
    //         cubeLines[i] = Line(faces2D[i], faces2D[(i+1)%4]);
    //         cubeLines[i+4] = Line(faces2D[i+4], faces2D[(i+1)%4 + 4]);
    //         cubeLines[i+8] = Line(faces2D[i], faces2D[i+4]);
    //     }

    //     // cubeLines = {
    //     //     Line(faces2D[0], faces2D[1]),
    //     //     Line(faces2D[1], faces2D[2]),
    //     //     Line(faces2D[2], faces2D[3]),
    //     //     Line(faces2D[3], faces2D[0]),
    //     //     Line(faces2D[4], faces2D[5]),
    //     //     Line(faces2D[5], faces2D[6]),
    //     //     Line(faces2D[6], faces2D[7]),
    //     //     Line(faces2D[7], faces2D[4]),
    //     //     Line(faces2D[0], faces2D[4]),
    //     //     Line(faces2D[1], faces2D[5]),
    //     //     Line(faces2D[2], faces2D[6]),
    //     //     Line(faces2D[3], faces2D[7]),
    //     // };


    //     // VERTICAL PLANE
    //     plane2D = Project<4>(plane3D);
    //     for(int i=0; i<4; i++){
    //         planeLines[i] = Line(plane2D[i], plane2D[(i+1)%4]);
    //     }

    //     window->clear();

    //     // window->draw(&point, 5, sf::Points);
    //     // window->draw(line, 2, sf::Lines);
    //     // for( auto obj : _triangles)
    //     //     window->draw(obj);

    //     // for( auto obj : _rectangles)
    //     //     window->draw(obj);
    //     //window->draw(points2D[0]);
        
    //     // CUBE
    //     // for(Vector2D p : faces2D){
    //     //     window->draw(Point(p));
    //     // }
    //     // for(Line line : cubeLines){
    //     //     window->draw(line);
    //     // }
        
    //     // VERTICAL PLANE
    //     for(Vector2D p : plane2D){
    //         window->draw(Point(p));
    //     }
    //     for(Line line : planeLines){
    //         window->draw(line);
    //     }


    //     window->display();

    // }

    return 0;
}