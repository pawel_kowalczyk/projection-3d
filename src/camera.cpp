#include "../inc/camera.hh"

Camera::Camera() : CoordSystem(){}

Camera::Camera(CoordSystem* parent, const Vector3D& translation, const MatrixRot& rotation)
        : CoordSystem(parent, translation, rotation) {}
