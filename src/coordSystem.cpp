#include "../inc/coordSystem.hh"

CoordSystem::CoordSystem(){
    _parent = nullptr;
    _translation = {0,0,0};
    _rotation = MatrixRot('X', 0);
}

CoordSystem::CoordSystem(CoordSystem* parent, const Vector3D& translation, const MatrixRot& rotation){
    _parent = parent;
    _translation = translation;
    _rotation = rotation;
}

void CoordSystem::Translate(const Vector3D& vec){
    _translation += vec;
}

void CoordSystem::Rotate(const MatrixRot& matrix){
    _rotation *= matrix;
}

void CoordSystem::RotateX(const double angle){
    _rotation *= MatrixRot('X', angle*M_PI/180);
}
void CoordSystem::RotateY(const double angle){
    _rotation *= MatrixRot('Y', angle*M_PI/180);
}
void CoordSystem::RotateZ(const double angle){
    _rotation *= MatrixRot('Z', angle*M_PI/180);
}

Vector3D CoordSystem::CalculateToParent(const Vector3D& point) const{
    if( _parent == nullptr )
        return _rotation * point + _translation;
    else
        return _parent->CalculateToParent( _rotation * point + _translation );
}

CoordSystem* CoordSystem::GetParent() const{
    return _parent;
}
Vector3D CoordSystem::GetTranslation() const{
    return _translation;
}
MatrixRot CoordSystem::GetRotation() const{
    return _rotation;
}
