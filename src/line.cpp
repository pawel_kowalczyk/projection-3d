#include "../inc/line.hh"

Line::Line(){
    _points[0] = Vector2D();
    _points[1] = Vector2D();
}

Line::Line(const Point& p1, const Point& p2, const sf::Color color){
    _points[0] = sf::Vertex(sf::Vector2f(p1[0], p1[1]), color);
    _points[1] = sf::Vertex(sf::Vector2f(p2[0], p2[1]), color);

}

void Line::draw(sf::RenderTarget& target, sf::RenderStates states) const{
    target.draw(_points, 2, sf::Lines, states);
}