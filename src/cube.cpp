#include "../inc/cube.hh"

// 0 < size < 1
Cube::Cube(CoordSystem* parent, const Vector3D& translation, const MatrixRot& rotation, const double size) 
    : Cuboid(parent, translation, rotation, {size, size, size}){
}
