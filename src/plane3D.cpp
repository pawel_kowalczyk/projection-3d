#include "../inc/plane3D.hh"

Plane3D::Plane3D() : Object3D() {}

Plane3D::Plane3D(CoordSystem* parent, const Vector3D& translation, const MatrixRot& rotation) 
        : Object3D(parent, translation, rotation){

    const double s = 10;

    SetVertices( {
        Vector3D{-s, -s, 0},
        Vector3D{-s,  s, 0},
        Vector3D{ s,  s, 0},
        Vector3D{ s, -s, 0},
    });

}