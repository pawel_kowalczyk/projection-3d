#include "../inc/rectangle.hh"

Rectangle::Rectangle(){}

Rectangle::Rectangle(const Point& p1, const double w, const double h){
    _triangles[0] = RectangularTriangle( p1, w, h );
    _triangles[1] = RectangularTriangle(  {p1[0] + w, p1[1] + h}, -w, -h );
}

void Rectangle::draw(sf::RenderTarget& target, sf::RenderStates states) const{
    for(int i=0; i<2; ++i)
        target.draw( _triangles[i] );
}
    
